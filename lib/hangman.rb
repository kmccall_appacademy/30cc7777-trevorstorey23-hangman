class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players = {referee: HumanPlayer.new, guesser: ComputerPlayer.new})
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    @secret_word_length = referee.pick_secret_word
    @guesser.register_secret_length(@secret_word_length)
    @board = []
    (@secret_word_length).times { @board << "_" }
    display_board
  end

  def display_board
    puts @board.join("")
  end
  #
  def take_turn
  letter_guess = @guesser.guess(@board)
  indices_in_secret_word = @referee.check_guess(letter_guess)
  update_board(letter_guess, indices_in_secret_word)
  @guesser.handle_response(letter_guess, indices_in_secret_word, @board)
  end

  def update_board(letter, indices)
    @board.each_with_index do |char, i|
      if indices.include?(i)
        @board[i] = letter
      end
    end
    display_board
  end

  def won?
    !@board.include? "_"
  end

  def play
    setup
    until won?
      take_turn
    end
    return "You guessed it!"
  end

end


class HumanPlayer

  def pick_secret_word
    puts "What is the length of the word you're thinking of?"
    @secret_word_length = gets.chomp.to_i
  end

  def guess(board)
    puts "What letter is your guess?"
    letter = gets.chomp
    letter
  end

  def check_guess(letter)
    puts "If not, just press enter. Otherwise please enter the indices where it is in your word."
    answer = gets.chomp.split(" ")
    indices = answer.map { |index| index.to_i }
    indices
  end

  def handle_response(arg1, arg2, arg3)
  end

  def register_secret_length(not_used)
  end

end

class ComputerPlayer

  def initialize
    @dictionary = []
    @possible_words = []
  end

  def read_dictionary
    File.foreach("lib/dictionary.txt") { |word| @dictionary << word.chomp }
  end
  #
  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end
  #
  def check_guess(letter)
    indices = []
    @secret_word.each_char.with_index do |chr, i|
      indices << i if chr == letter
    end
    indices
  end
  #
  def register_secret_length(length)
    @possible_words = @dictionary.select { |word| word.length == length }
    @possible_chars = @possible_words.join("").split("")
  end
  #
  def guess(board)
    # p @possible_words
    letter = most_common_letter(@possible_chars)
    puts "Are there any '#{letter}'s in your word?"
    letter
  end

  def handle_response(letter, indices, board)
    if indices.empty?
      @possible_words.reject! { |word| word.include? letter }
    else
      @possible_words.select! do |word|
        indices.all? { |index| word[index] == letter }
      end
    end
    @possible_chars = @possible_words.join("").split("")
    @possible_chars.select! { |chr| !board.include? chr }
  end

  def most_common_letter(characters)
    letter_count = {}
    characters.each do |char|
      if letter_count[char]
        letter_count[char] += 1
      else
        letter_count[char] = 1
      end
    end
    letter_count.max_by{|_,v| v}[0]
  end

end
